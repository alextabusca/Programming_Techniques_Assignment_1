package controllers;

import views.*;
import models.*;
import javax.swing.*;
import java.awt.event.*;

public class Control {

    // The controller interacts with the GUI interface and the model - in this
    // case the Poly class
    private GUI view;
    Poly polinom1 = new Poly();
    Poly polinom2 = new Poly();
    Poly pol = new Poly();

    public Control(GUI gui) {
        view = gui;

        // Add listeners to the view
        view.addSetListener(new SetListener());
        view.addAdditionListener(new AdditionListener());
        view.addsubtractionListener(new SubtractionListener());
        view.addmultiplyListener(new MultiplyListener());
        view.adddivideListener(new DivideListener());
        view.addintegrateListener(new IntegrateListener());
        view.adddifferentiationListener(new DifferentiateListener());

    }

    class SetListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            try {
                polinom1 = pol.createPoly(view.polinom1.getText());
                polinom2 = pol.createPoly(view.polinom2.getText());
                System.out.println("Set Done!!");
                view.polinomial1.setText(polinom1.polinomToString());
                view.polinomial2.setText(polinom2.polinomToString());
            } catch (NullPointerException ez) {
                JOptionPane.showMessageDialog(null, "INVALID INPUT! PLEASE CORRECT AND TRY AGAIN!");
            }

        }
    }

    class AdditionListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (polinom1.grad >= polinom2.grad)
                view.textResult.setText(polinom1.addPoly(polinom2).polinomToString());
            else
                view.textResult.setText(polinom2.addPoly(polinom1).polinomToString());
        }
    }

    class SubtractionListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            Poly neg = new Poly();
            Mono minus = new MonoInt(-1, 0);
            Poly polminus = new Poly();
            polminus.insertMonom(minus);
            if (polinom1.grad >= polinom2.grad)
                view.textResult.setText(polinom1.subtractPoly(polinom2).polinomToString());
            else {
                neg = polinom2.multiplyPoly(polminus);
                view.textResult.setText(neg.addPoly(polinom1).polinomToString());
            }
        }
    }

    class MultiplyListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            view.textResult.setText(polinom1.multiplyPoly(polinom2).polinomToString());
        }
    }

    class DivideListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            // Nothing yet unfortunately
        }
    }

    class IntegrateListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            view.textResult.setText(polinom1.integrate().polinomToString());
        }
    }

    class DifferentiateListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            view.textResult.setText(polinom1.differentiate().polinomToString());
        }
    }

}
