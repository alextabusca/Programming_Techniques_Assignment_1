package models;

import java.util.Comparator;

public class MonoInt extends Mono {

    public MonoInt(Integer coeff, int degree) {
        // TODO Auto-generated constructor stub
        super(coeff, degree);
    }

    public void setCoeff(Number coeff) {
        this.coeff = coeff;
    }

    public Number getCoeff() {
        return coeff;
    }

    public Mono addMonoms(Mono other) {
        if (this.degree != other.degree)
            throw new IllegalArgumentException("Degree must match");
        return new MonoInt((this.coeff.intValue() + other.coeff.intValue()), this.degree);
    }

    public Mono subtractMonoms(Mono other) {
        if (this.degree != other.degree)
            throw new IllegalArgumentException("Degree must match");
        return new MonoInt((this.coeff.intValue() - other.coeff.intValue()), this.degree);
    }

    public Mono multiplyMonoms(Mono other) {
        return new MonoInt((this.coeff.intValue() * other.coeff.intValue()), this.degree + other.degree);
    }

}