package models;

public class MonoReal extends Mono {

    public MonoReal(Double coeff, int degree) {
        // TODO Auto-generated constructor stub
        super(coeff, degree);
    }

    public void setCoeff(Number coeff) {
        this.coeff = coeff;
    }

    public Number getCoeff() {
        return coeff;
    }

    public Mono addMonoms(Mono other) {
        // if (this.degree != other.degree)
        // throw new IllegalArgumentException("Degree must match");
        return new MonoReal((this.coeff.doubleValue() + other.coeff.doubleValue()), this.degree);
    }

    public Mono subtractMonoms(Mono other) {
        // if (this.degree != other.degree)
        // throw new IllegalArgumentException("Degree must match");
        return new MonoReal((this.coeff.doubleValue() - other.coeff.doubleValue()), this.degree);
    }

    public Mono multiplyMonoms(Mono other) {
        return new MonoReal((this.coeff.doubleValue() * other.coeff.doubleValue()), this.degree + other.degree);
    }
}