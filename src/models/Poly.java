package models;

import java.util.ArrayList;
//import java.util.Collections;
//import java.util.Comparator;

public class Poly {

    private ArrayList<Mono> polinomial;
    public int grad;

    public Poly() {
        this.polinomial = new ArrayList<Mono>();
    }

    public void insertMonom(Mono monom) {
        polinomial.add(monom);
    }

    // Create the polynomial from the read coefficients
    public Poly createPoly(String text) {
        Poly polinom = new Poly();
        String[] stringArray = text.split(",");
        int[] coeffArray = new int[stringArray.length];
        int degree = stringArray.length - 1;
        polinom.grad = stringArray.length;

        for (int i = 0; i < stringArray.length; i++) {
            String numberAsString = stringArray[i];
            coeffArray[i] = Integer.parseInt(numberAsString);
            polinom.insertMonom(new MonoInt(coeffArray[i], degree));
            degree--;
        }

        return polinom;
    }

    public void interchange(Poly pol1, Poly pol2) {

    }

    // Add two polynomials
    public Poly addPoly(Poly other) {
        Poly temp = new Poly();
        for (Mono a : this.polinomial) {
            boolean added = false;
            for (Mono b : other.polinomial) {
                if (a.degree == b.degree) {
                    temp.insertMonom(a.addMonoms(b));
                    added = true;
                }
            }
            if (!added)
                temp.insertMonom(a);

        }
        return temp;
    }

    // Subtract two polynomials
    public Poly subtractPoly(Poly other) {
        Poly temp = new Poly();
        for (Mono a : this.polinomial) {
            boolean subtracted = false;
            for (Mono b : other.polinomial) {

                if (a.degree == b.degree) {
                    temp.insertMonom(a.subtractMonoms(b));
                    subtracted = true;
                }
            }

            if (!subtracted)
                temp.insertMonom(a);
        }

        return temp;
    }

    // Method for sorting the polynomial
	/*
	 * public static Comparator<Mono> sortDegree = new Comparator<Mono>() {
	 *
	 * @Override public int compare(Mono m1, Mono m2) { int grad1 = m1.degree;
	 * int grad2 = m2.degree;
	 *
	 * // Descending order return grad2 - grad1; }
	 *
	 * };
	 *
	 * public class OrderByDegree implements Comparator<Mono> {
	 *
	 * @Override public int compare(Mono m1, Mono m2) { return m2.degree -
	 * m1.degree; } }
	 */

    // Multiply two polynomials
    public Poly multiplyPoly(Poly other) {
        Poly rez = new Poly();
        Poly rezultat = new Poly();
        Poly aux = new Poly();
        rez.insertMonom(new MonoInt(0, 0));
        for (Mono a : this.polinomial) {
            //Mono auxMono = new MonoInt(0, 1);
            //rez.insertMonom(auxMono);
            for (Mono b : other.polinomial) {
                rezultat.insertMonom(a.multiplyMonoms(b));
                //Mono auxiliaryMono = a.multiplyMonoms(b);
                //aux.insertMonom(auxiliaryMono);
            }
            //rezultat = rez.addPoly(aux);
        }

        //or(Mono a : rez.polinomial)
        //for(Mono b : rez.polinomial)
        //if(a.degree == b.degree)
        return rezultat;

    }

    // Differentiate a polynomial
    public Poly differentiate() {
        Poly temp = new Poly();
        for (Mono a : this.polinomial) {
            int c = 0, g = 0;
            c = a.coeff.intValue() * a.degree;
            g = a.degree - 1;
            temp.insertMonom(new MonoInt(c, g));
        }
        return temp;
    }

    // Integrate a polynomial
    public Poly integrate() {
        Poly temp = new Poly();
        for (Mono a : this.polinomial) {
            int g = 0;
            double c = 0;
            c = a.coeff.doubleValue() / (a.degree + 1);
            g = a.degree + 1;
            if (c % 1 == 0)
                temp.insertMonom(new MonoInt((int) c, g));
            else
                temp.insertMonom(new MonoReal(c, g));
        }
        return temp;
    }

    // toString method for building the polynomial
    public String polinomToString() {
        final StringBuilder sb = new StringBuilder();
        for (Mono m : polinomial) {
            if (m.coeff.doubleValue() != 0) {
                if (m.coeff.doubleValue() > 0)
                    sb.append('+');
                sb.append(m.coeff);
            }

            if (m.degree != 0 && m.coeff.doubleValue() != 0) {
                sb.append('x');
                if (m.degree != 1)
                    sb.append('^').append(m.degree);
            }
        }
        return sb.toString();
    }

}