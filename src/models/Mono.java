package models;

//import java.util.Comparator;

/**
 *
 * @author Alex The monomial class
 */

public abstract class Mono /* implements Comparable<Mono> */ {

    protected Number coeff;
    protected int degree;

    public Mono(Number coeff, int degree) {
        this.coeff = coeff;
        this.degree = degree;
    }

    public void setDegree(int degree) {
        this.degree = degree;
    }

    public int getDegree() {
        return degree;
    }

    public abstract void setCoeff(Number coeff);

    public abstract Number getCoeff();

    public abstract Mono addMonoms(Mono other);

    public abstract Mono subtractMonoms(Mono other);

    public abstract Mono multiplyMonoms(Mono other);

}