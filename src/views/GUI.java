package views;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class GUI extends JFrame {

    private static final long serialVersionUID = 1L;
    // Components of the frame
    private JPanel p1;
    private JPanel p2;
    private JPanel setpol;
    private JPanel result;
    private JPanel operations;
    private JPanel panelFinal;
    private JPanel showPol;

    private JLabel pol1 = new JLabel("Polynomial 1: ");
    private JLabel pol2 = new JLabel("Polynomial 2: ");
    private JLabel pol11 = new JLabel("Polynomial 1: ");
    private JLabel pol22 = new JLabel("Polynomial 2: ");
    private JLabel res = new JLabel("Result: ");
    public JLabel polinomial1 = new JLabel();
    public JLabel polinomial2 = new JLabel();

    public JTextField polinom1 = new JTextField(35);
    public JTextField polinom2 = new JTextField(35);
    public JTextField textResult = new JTextField(50);

    private JButton set = new JButton("Set");
    private JButton add = new JButton("Addition");
    private JButton sub = new JButton("Subtraction");
    private JButton mul = new JButton("Multiply");
    private JButton div = new JButton("Divide");
    private JButton integrate = new JButton("Integrate");
    private JButton differentiate = new JButton("Differentiate");

    public GUI() {

        p1 = new JPanel();
        p1.setLayout(new FlowLayout());
        p1.add(pol1);
        p1.add(polinom1);

        p2 = new JPanel();
        p2.setLayout(new FlowLayout());
        p2.add(pol2);
        p2.add(polinom2);

        setpol = new JPanel();
        setpol.setLayout(new GridLayout(1, 1));
        setpol.add(set);

        showPol = new JPanel();
        showPol.setLayout(new GridLayout(2, 2));
        showPol.add(pol11);
        showPol.add(polinomial1);
        showPol.add(pol22);
        showPol.add(polinomial2);

        result = new JPanel();
        result.setLayout(new FlowLayout());
        result.add(res);
        result.add(textResult);

        operations = new JPanel();
        operations.setLayout(new GridLayout(1, 6));
        operations.add(add);
        operations.add(sub);
        operations.add(mul);
        operations.add(div);
        operations.add(integrate);
        operations.add(differentiate);

        panelFinal = new JPanel();
        panelFinal.setLayout(new BoxLayout(panelFinal, BoxLayout.PAGE_AXIS));
        panelFinal.add(p1);
        panelFinal.add(p2);
        panelFinal.add(set);
        panelFinal.add(showPol);
        panelFinal.add(Box.createVerticalStrut(15));
        panelFinal.add(operations);
        panelFinal.add(Box.createVerticalStrut(15));
        panelFinal.add(result);

        this.setContentPane(panelFinal);
        this.setTitle("Polynomial Operations");
        this.pack();
        setSize(700, 300);
        setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

    public void addSetListener(ActionListener setListener) {
        set.addActionListener(setListener);
    }

    public void addAdditionListener(ActionListener additionListener) {
        add.addActionListener(additionListener);
    }

    public void addsubtractionListener(ActionListener subtractionListener) {
        sub.addActionListener(subtractionListener);
    }

    public void addmultiplyListener(ActionListener multiplyListener) {
        mul.addActionListener(multiplyListener);
    }

    public void adddivideListener(ActionListener divideListener) {
        div.addActionListener(divideListener);
    }

    public void addintegrateListener(ActionListener integrateListener) {
        integrate.addActionListener(integrateListener);
    }

    public void adddifferentiationListener(ActionListener differentiationListener) {
        differentiate.addActionListener(differentiationListener);
    }

}
